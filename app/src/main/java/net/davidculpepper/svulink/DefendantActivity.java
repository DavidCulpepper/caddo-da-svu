package com.csc490.bknotek.svuprogram;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

public class DefendentActivity extends AppCompatActivity {

    EditText firstName;
    EditText middleName;
    EditText lastName;
    EditText dateOfBirth;
    EditText licenseNum;
    EditText probationOfficer;

    ToggleButton probationStatus;
    Button addDefendants;
    RadioGroup childrenGroup;
    RadioGroup victimGroup;
    RadioGroup publicGroup;

    Button submitButton;
    Button editButton;

    LinearLayout readonlyView;
    LinearLayout editView;

    TextView firstNameReadOnly;
    TextView middleNameReadOnly;
    TextView lastNameReadOnly;
    TextView dateofBirthReadOnly;
    TextView licenseNumReadOnly;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_defendant);

        firstName = (EditText)findViewById(R.id.first_name);
        middleName = (EditText)findViewById(R.id.middle_name);
        lastName = (EditText)findViewById(R.id.last_name);
        dateOfBirth = (EditText)findViewById(R.id.date_of_birth);
        licenseNum = (EditText)findViewById(R.id.license_name);
        probationOfficer = (EditText)findViewById(R.id.probation_officer);

        probationStatus = (ToggleButton)findViewById(R.id.probation_toggle_button);
        addDefendants = (Button)findViewById(R.id.add_defendant);
        childrenGroup = (RadioGroup)findViewById(R.id.children_radio_group);
        victimGroup = (RadioGroup)findViewById(R.id.victim_radio_group);
        publicGroup = (RadioGroup)findViewById(R.id.victim_radio_group);

        submitButton = (Button)findViewById(R.id.submit_button);

        readonlyView = (LinearLayout)findViewById(R.id.read_only_view);
        editView = (LinearLayout)findViewById(R.id.edit_view);

        editButton = (Button)findViewById(R.id.edit_button);

        firstNameReadOnly = (TextView)findViewById(R.id.first_name_readOnly);
        middleNameReadOnly = (TextView)findViewById(R.id.middle_name_readOnly);
        lastNameReadOnly = (TextView)findViewById(R.id.last_name_readOnly);
        dateofBirthReadOnly = (TextView)findViewById(R.id.date_of_birth_readOnly);
        licenseNumReadOnly = (TextView)findViewById(R.id.licenseNum_readOnly);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstNameReadOnly.setText(firstName.getText());
                middleNameReadOnly.setText(middleName.getText());
                lastNameReadOnly.setText(lastName.getText());
                dateofBirthReadOnly.setText(dateOfBirth.getText());
                licenseNumReadOnly.setText(licenseNum.getText());
                editView.setVisibility(View.GONE);
                readonlyView.setVisibility(View.VISIBLE);

            }
        });

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                readonlyView.setVisibility(View.GONE)
                editView.setVisibility(View.VISIBLE);



            }
        });

    }
}
